package com.company.keo_kay;


import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int choice=0;
        Scanner input= new Scanner(System.in);


        String url="jdbc:postgresql://localhost:5432/postgres";
        String user="postgres";
        String pass=" keokay@admin";
        Properties properties= new Properties();
        properties.setProperty("pass"," keokay@admin");
        properties.setProperty("user","postgres");


       int id=0;


        do {

            System.out.println("======================= Working with Postgres SQL ======================");
            System.out.println("1. Display All Records ");
            System.out.println("2. Insert New Records ");
            System.out.println("3. Update Records (Student's Name)");
            System.out.println("4. Delete Record  ");
            System.out.println("5. Exit \n");

            System.out.print("Option : "); choice=input.nextInt();

            switch (choice){

                case 1:
                    // Display record
                    display(url,user,pass);
                    break;
                case 2:
                    // Insert New Record
                    insert(url, user,pass);

                    break;
                case 3:
                    update(url,user,pass);
                    // Update Record
                    break;
                case 4:
                    // Delete record
                    delete(url,user,pass);
                    break;

            }
        }while (choice!= 5);





    }

    public static void  delete(String url , String user, String pass){
        Scanner input = new Scanner(System.in);
        int id=0;
        String name;
        String gender;
        int   cardId;
        int generationID;


        // Update Testing
        //   String insertSQL="INSERT INTO  student_table(id,name,gender,card_id,generation_id)  VALUES(?,?,?,?,?)";
        String deleteSQL="DELETE FROM student_table  where id=?";
        try(
                Connection connection= DriverManager.getConnection(url,user,pass);
                PreparedStatement pst= connection.prepareStatement(deleteSQL)


        ){

            System.out.print("Enter the ID            : "); id= input.nextInt();


            pst.setInt(1, id);

            pst.executeUpdate();


        }catch (SQLException ex){

            System.out.println(ex.getMessage());
        }


    }






    public static void  update(String url , String user, String pass){
        Scanner input = new Scanner(System.in);
        int id=0;
        String name;
        String gender;
        int   cardId;
        int generationID;


        // Update Testing
     //   String insertSQL="INSERT INTO  student_table(id,name,gender,card_id,generation_id)  VALUES(?,?,?,?,?)";
        String updateSQL="update student_table set name =? , gender=? where id=?";
        try(
                Connection connection= DriverManager.getConnection(url,user,pass);
                PreparedStatement pst= connection.prepareStatement(updateSQL)


        ){

            System.out.print("Enter the ID            : "); id= input.nextInt(); input.nextLine();
            System.out.print("Enter the Name          :"); name=input.nextLine();
           System.out.print("Enter Gender            : ");gender= input.next();
         //   System.out.print("Enter Card ID           : ");cardId= input.nextInt();
          //  System.out.print("Enter the Generation ID : ");generationID=input.nextInt();

            pst.setInt(3, id);
             pst.setString(1,name);
             pst.setString(2,gender);
         //   pst.setInt(4, cardId);
   //         pst.setInt(5,generationID);

            pst.executeUpdate();


        }catch (SQLException ex){

            System.out.println(ex.getMessage());
        }


    }




    public static void insert(String url , String user, String pass){
        Scanner input = new Scanner(System.in);
        int id=0;
        String name;
        String gender;
        int   cardId;
        int generationID;


        // Insert Testing
        String insertSQL="INSERT INTO  student_table(id,name,gender,card_id,generation_id)  VALUES(?,?,?,?,?)";
        try(
                Connection connection= DriverManager.getConnection(url,user,pass);
                PreparedStatement pst= connection.prepareStatement(insertSQL)


        ){

            System.out.print("Enter the ID            : "); id= input.nextInt(); input.nextLine();
            System.out.print("Enter the Name          :"); name=input.nextLine();
            System.out.print("Enter Gender            : ");gender= input.next();
            System.out.print("Enter Card ID           : ");cardId= input.nextInt();
            System.out.print("Enter the Generation ID : ");generationID=input.nextInt();

            pst.setInt(1, id);
            pst.setString(2,name);
            pst.setString(3,gender);
            pst.setInt(4, cardId);
            pst.setInt(5,generationID);

            pst.executeUpdate();


        }catch (SQLException ex){

            System.out.println(ex.getMessage());
        }


    }

    public static void display(String url, String user  ,String pass){


        int count=0;
        String sqlStatement = "SELECT * FROM student_table";

        try(
                Connection connection= DriverManager.getConnection(url,user,pass);
                Statement stm = connection.createStatement();
                ResultSet rs= stm.executeQuery(sqlStatement);

        ){

//            rs.next();
//            count= rs.getInt(1);
//            System.out.println("Here is the count value : "+count);


            while (rs.next()){
                //  System.out.println(rs.getInt(1) +"  "+ rs.getInt(2) + rs.getInt(3));
                System.out.println(rs.getString("id") +"\t"+rs.getString("name") + "\t"+rs.getString("gender") + "\t" + rs.getString("card_id")+ "\t" + rs.getString("generation_id"));
            }

        // it close by itself
        } catch (SQLException exception){

            System.out.println(exception.getMessage());
        }


    }
}
